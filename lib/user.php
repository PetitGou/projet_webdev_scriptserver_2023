<?php


function getUser(string $field, string $value): mixed
{

    if (!in_array($field, getColumns('user'))) {
        return false;
    }

    $connect = connect();

    // 2. QUERY
    $request = $connect->prepare("SELECT * FROM user WHERE $field = ?");

    $params = [
        trim($value),
    ];

    // 3. EXECUTE
    $request->execute($params);

    // 4. FETCH
    return $request->fetchObject();
}

/**
 * @return array|false
 */
function getallUsers(){

    $connect = connect();

    $allusers = $connect->prepare('SELECT * FROM user');

    $allusers->execute();

    $all = $allusers->fetchAll();

    return $all;
}


function userExists(string $field, string $value): bool
{
    if (is_object(getUser($field, $value))) {
        return true;
    } else {
        return false;
    }
}



function logout(): void
{
    session_unset();
    session_destroy();
    session_write_close();

    header('Location: index.php');
}