<?php

function getAdmin(){
    global $connect;

    $sql = 'SELECT * FROM user WHERE admin = 1';

    $connect = connect();

    $status = $connect->prepare($sql);

    $status->execute();

   if($status->rowCount() == 0) {
       return 1;
   }else {
       return 0;
   }
}


function getStatus($status) {
    if($status == 0) {
        return 'Non';
    }else {
        return 'Oui';
    }
}
function getRole(array $params) {
    global $connect;

    $connect = connect();

    $sql = 'SELECT admin FROM user WHERE id = ?';

    $query = $connect->prepare($sql);

    $query->execute($params);

    $result = $query->fetchColumn();

    return $result;



}