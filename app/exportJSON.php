<?php



if(!empty($_SESSION['userid'])){
ob_clean();
global $connect;

$infos = $connect->prepare('SELECT * FROM user WHERE id = ?');
$infos->execute([$_SESSION['userid']]);
$userjson = $infos->fetchObject();

$filename = $userjson->username . '_' . time() .'.json';
// Envoi des headers HTTP au browser pour le téléchargement du fichier.
//Permet de gérer l'affichage
header('Content-type: application/json');
//Permet de télécharger le fichier avec le nom qu'on a spécifié
header('Content-disposition: attachment; filename="' . $filename . '"');

}

echo json_encode($userjson);
