<?php

//Traitement de la création d'un nouvel utilisateur


include_once './lib/roles.php';

$url = 'index.php?page=view/create';

if (!empty($_POST['username']) && !empty($_POST['password']) && filter_var($_POST['email'], FILTER_VALIDATE_EMAIL)) {


    if (userExists('username', $_POST['username'])) {
        $_SESSION['alert'] = 'Ce nom d\'utilisateur est déjà utilisé';
        header('Location: index.php?page=view/create');
    }
    if (userExists('email', $_POST['email'])) {
        $_SESSION['alert'] = 'L\'email est déjà utilisé';
        header( 'Location: index.php?page=view/create');
    }


    // Stockage des informations sorties du formulaire dans des variables (Fait ainsi car plus simple d'utilisation et sécurité pour les injection SQl)
    $login = $_POST['username'];
    $pwd = $_POST['password'];
    $email = $_POST['email'];


    $admin = getAdmin();

    // on détermine les paramètres à passer à la requête SQL PDO
    $params = [
        trim($login),
        password_hash($pwd, PASSWORD_DEFAULT),
        $email,
        $admin,
    ];


    //Certain serveur de sql n'acceptent pas NOW() pour un champ date-time alors on initialise dans la variable $now la date

    global $connect;

    $connect = connect();

    $create = $connect->prepare("INSERT INTO user( username, password, email,admin, lastlogin, created ) VALUES (? , ? , ?, ?, NOW(), NOW())");

    $create->execute($params);

    if ($create->rowCount()) {
        $userid = $connect->LastInsertId();
        $_SESSION['alert'] = 'L\'utilisateur ' . $login . ' a été créé avec succès';
        $_SESSION['alert-color'] = 'success';
        $url = 'index.php?page=view/loginform';
    } else {
        $_SESSION['alert'] = 'La création de l\'utilisateur a échoué';
    }
} else {
    $_SESSION['alert'] = 'La création a échoué';

}
header('Location: ' . $url);

die;