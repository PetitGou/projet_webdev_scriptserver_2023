<?php


// Traitement du formulaire de login après la création d'un nouvel utilisateur

if (!empty($_POST['username']) && !empty($_POST['password'])) {


//Vérifie si le 'username' insérer dans le formulaire correspond à un username en DB
    $login = trim($_POST['username']);
    $user = getUser('username', $_POST['username']);
    if ($user) {
// Utilisation de la fonction native PHP password_verify pour valider un mot de passe et sa valeur de hashage stockée en DB par la fonction native password_hash
        if (password_verify($_POST['password'], $user->password)) { //Verifier aussi l'email
            // L'id de l'utilisateur est stocké en session
            if (!empty($user->id)) {
                $_SESSION['userid'] = $user->id;
                // Si le mot de passe est vérifié, on met à jour le champ "lastlogin" dans la table user en DB
                $sql = "UPDATE user SET lastlogin = NOW() WHERE id = ?";
                $connect = connect();
                $update = $connect->prepare($sql);
                $update->execute([$user->id]);
                $_SESSION['alert'] = 'Bienvenue ' . $user->username;
                $_SESSION['alert-color'] = 'success';


            }
            $url = 'index.php?page=view/profile';

            if ($user->admin == 1) {
                $url = 'index.php?page=view/admin';
            } else {
                $url = 'index.php?page=view/profile';
            }

        } else {
            $_SESSION['alert'] = 'Echec de l\'authentification';
            $url = 'index.php?page=view/loginform';
        }
    } else {
    $_SESSION['alert'] = 'Utilisateur non trouvé';
    $url = 'index.php?page=view/loginform';
}
    header('Location: ' . $url);

}