<?php

$go_out = false;
$update = false;
$url = 'index.php?page=view/loginform';

if ($go_out) {
    header('Location :' . $url);
    $_SESSION['alert'] = 'Une erreur s\'est produite... Reconnectez-vous !';
    $_SESSION['alert-color'] = 'danger';
    die;
}

if (!empty($_SESSION['userid'])) {
    $user = getUser('id', $_SESSION['userid']);

    if (!is_object($user)) {
        $go_out = true;
        die;
    } else {
        if ($_POST != $user) {
            $params = [];

            $connect = connect();

            if (!empty($_POST['email']) && !empty($_POST['password'])) {
                if (filter_var($_POST['email'], FILTER_VALIDATE_EMAIL)) {
                    $params = [
                        $_POST['email'],
                        password_hash($_POST['password'], PASSWORD_DEFAULT),
                        $user->id,
                    ];
                } else {
                    $_SESSION['alert'] = 'L\'email n\'est pas valide';
                }
                $update = $connect->prepare("UPDATE user SET email = ? , password = ? WHERE id = ? ");

            } elseif (empty($_POST['email']) && !empty($_POST['password'])) {
                $params = [
                    password_hash($_POST['password'], PASSWORD_DEFAULT),
                    $user->id,
                ];

                $update = $connect->prepare("UPDATE user SET password = ? WHERE id = ? ");

            } elseif (!empty($_POST['email']) && empty($_POST['password'])) {
                if (filter_var($_POST['email'], FILTER_VALIDATE_EMAIL)) {

                    $params = [
                        $_POST['email'],
                        $user->id,
                    ];
                } else {
                    $_SESSION['alert'] = 'L\'email n\'est pas valide';
                }
                $update = $connect->prepare("UPDATE user SET email = ? WHERE id = ? ");
            } else {
                $_SESSION['alert'] = 'Modification échouer car vous n\'avez renseigner aucuns champs';
                $url = 'index.php?page=view/profile';
                die;
            }

            $update->execute($params);

            if ($update->rowCount()) {
                echo 'L\'utilisateur ' . $user->username . ' a été modifer avec succès !';
                $url = 'index.php?page=view/profile';
            } else {
                echo 'La modification a échouer veuillez réessayer ';
                $url = 'index.php?page=view/profile';
            }
        }
    }

} else {

    $go_out = true;
}

header('Location: ' . $url);
