<?php

$output = '<h2>Liste des cours</h2>
<table class="table">
    <thead>
        <tr>
            <th>Intitulé</th>
            <th>Code du cours</th>
        </tr>
    </thead>
    <tbody>';

$sql = 'SELECT * FROM course ORDER BY name ASC';


global $connect;

$courses = $connect->prepare($sql);

$courses->execute();

$courses->fetchObject();

if (!empty($courses)) {

    foreach ($courses as $key => $value) {

        if ($key == 'name') {
            $value = $key['name'];
        } elseif ($key == 'code') {
            $value = $key['code'];
        }

        $output .= '<tr><td>' . ucfirst($value['name']) . '</td><td>' . $value['code'] . '</td></tr>';
    }

    $output .= '</tbody></table>';
}

echo $output;
