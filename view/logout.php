<?php


include_once './lib/user.php';


if (function_exists('logout')) {
    logout();
} else {
    header('Location: index.php');
    die;
}
