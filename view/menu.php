<nav class="nav">
    <a href="index.php" class="nav-link">Accueil</a>
    <a href="index.php?page=view/courseslist" class="nav-link">Liste des cours</a>
    <?php
    if (!empty($_SESSION['userid'])) {
        ?>
        <a href="index.php?page=view/profile" class="nav-link">Profil</a>
        <?php
        $user = getUser('id', $_SESSION['userid']);
        if ($user->admin) {
            ?>
            <a href="index.php?page=view/admin" class="nav-link">Admin</a>
            <?php
        }
        ?>
        <a href="index.php?page=view/logout" class="nav-link">Logout</a>
        <?php
    } else {
        ?>
        <a href="index.php?page=view/loginform" class="nav-link">Login</a>
        <?php
    }
    ?>
</nav>
