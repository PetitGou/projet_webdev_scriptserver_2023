<?php
include_once './lib/roles.php';

$go_out = false;

if(!empty($_SESSION['userid'])) {
    $user = getUser('id', $_SESSION['userid']);
    if(!is_object($user)){
        $go_out = true;
    }
} else {
    $go_out= true;
}

if($go_out) {
    header('Location: index.php?page=view/loginform');
    die;
}

$output = '<h2>Profil</h2>
<table class="table">
    <thead>
        <tr>
            <th>Intitulé</th>
            <th>Valeur</th>
        </tr>
    </thead>
    <tbody>';

if (!empty($user)) {
    foreach ($user as $key => $value) {
        if ($key == 'username') {
            $value = $user->username;
        } elseif ($key == 'email') {
            $value = $user->email;
        } elseif ($key == 'password' || $key == 'id') {
            continue;
        } elseif ($key == 'lastlogin' || $key == 'created') {
            $value = date_format(new DateTime($value), "d/m/Y H\hi");
        }elseif ($key == 'admin') {
            $value = getStatus($value);
        }
        $output .= '<tr><th>' . ucfirst($key) . '</th><td>' . $value . '</td></tr>';
    }
}
$output .='</tbody></table>';



echo $output ;

echo '<a class="btn btn-primary" href="index.php?page=app/exportJSON">Télécharger au format .JSON</a>';

include_once 'updateform.php';

