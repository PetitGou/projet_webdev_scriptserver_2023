<?php

$output = '';

if (!empty($user)) {
    $output = '<hr>
                 <h3><a href="#profile-update-collapse" data-bs-toggle="collapse" role="button" class="text-decoration-none">Update</a></h3>
                 <div class="collapse" id="profile-update-collapse">
                    <form action="index.php?page=app/update" method="post" enctype="multipart/form-data">
                        <input type="hidden" id="uu-userid" name="id" value="' . $user->id . '"><br>
                        <label for="uu-password">Password</label>
                        <input type="password" id="uu-password" name="password" class="form-control" value=""><br>
                        <label for="uu-email">Email</label>
                        <input type="email" id="uu-email" name="email" class="form-control" value="'. $user->email .'">
                        <input type="submit" class="btn btn-primary">
                    </form>
                 </div>';
}

echo $output;