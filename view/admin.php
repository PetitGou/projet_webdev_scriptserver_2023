<?php

// si un user qui n'est pas inscrit veut taper "admin" dans l'url
if (empty($_SESSION['userid'])) {
    header('Location: index.php?page=view/loginform');
    die;
}
// si l'user tape "admin" dans l'url sans etre admin
$user = getUser('id', $_SESSION['userid']);

if (!$user || $user->admin !== 1) {
    header('Location: index.php?page=view/profile');
    die;
}


$go_out = false;

if (!empty($_SESSION['userid'])) {
    $user = getUser('id', $_SESSION['userid']);
    if (!is_object($user)) {
        $go_out = true;
    }
} else {
    $go_out = true;
}

if ($go_out) {
    header('Location: index.php?page=view/loginform');
    die;
}
$users = getallUsers();


if ($users) {


    $output = '<h2>Users list</h2>
<table class="table">
    <thead>
        <tr>
            <th>Id</th>
            <th>Username</th>
            <th>Email</th>
            <th>Created</th>
            <th>Last login</th>
            <th>Admin</th>
            <th>Edit role</th>
        </tr>
    </thead>
    <tbody>';
    foreach ($users as $user) {
        if ($user['admin']) {
            $admin = 0;
            $caption = 'Role user';
        } else {
            $admin = 1;
            $caption = 'Role admin';
        }
        $btn = '<form action="index.php?page=app/editRole" method="post">
<input type="hidden" name="userid" value="' . $user['id'] . '">
<input type="hidden" name="admin" value="' . $user['admin'] . '">
<input type="submit" value="' . $caption . '" name="rolechange">
</form>';


        $output .= '<tr><td>' . $user['id'] .
            '</td><td>' . $user['username'] .
            '</td><td>' . $user['email'] .
            '</td><td>' . $user['created'] .
            '</td><td>' . $user['lastlogin'] .
            '</td><td>' . $user['admin'] .
            '</td><td>' . $btn . '</td></tr>';
    }

    echo $output;
    '</tbody>
</table>';
} else {
    echo 'Aucuns utilisateur présent dans la base de données';
}